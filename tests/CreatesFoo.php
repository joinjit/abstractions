<?php

namespace Jit\AbstrActions\Tests;

use Jit\AbstrActions\Tests\Models\Foo;

trait CreatesFoo
{
    protected function createFoo(): Foo
    {
        return Foo::create([ "name" => "Bar" ]);
    }
}
