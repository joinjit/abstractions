<?php

namespace Jit\AbstrActions\Tests\Unit;

use Jit\AbstrActions\Tests\Actions\Update\CrashingTestUpdateAction;
use Jit\AbstrActions\Tests\Actions\Update\TestUpdateAction;
use Jit\AbstrActions\Tests\CreatesFoo;
use Jit\AbstrActions\Tests\TestCase;

class UpdateActionTest extends TestCase
{
    use CreatesFoo;

    /** @test */
    public function it_updates_successfully()
    {
        $foo = $this->createFoo();

        $executed = (new TestUpdateAction())->execute($foo, [ "name" => "Baz" ]);

        $this->assertTrue($executed);
        $this->assertDatabaseMissing("foos", [ "name" => "Bar" ]);
        $this->assertDatabaseHas("foos", [ "name" => "Baz" ]);
    }

    /** @test */
    public function it_does_not_execute_if_data_is_invalid()
    {
        $foo = $this->createFoo();

        $executed = (new TestUpdateAction())->execute($foo);

        $this->assertFalse($executed);
        $this->assertDatabaseMissing("foos", [ "name" => "Baz" ]);
        $this->assertDatabaseHas("foos", [ "name" => "Bar" ]);
    }

    /** @test */
    public function it_does_not_execute_if_data_is_empty()
    {
        $foo = $this->createFoo();

        $executed = (new TestUpdateAction())->execute($foo, []);

        $this->assertFalse($executed);
        $this->assertDatabaseMissing("foos", [ "name" => "Baz" ]);
        $this->assertDatabaseHas("foos", [ "name" => "Bar" ]);
    }

    /** @test */
    public function it_does_not_execute_if_an_exception_is_thrown()
    {
        $foo = $this->createFoo();

        $executed = (new CrashingTestUpdateAction())
            ->execute($foo, [ "name" => "Baz" ]);

        $this->assertFalse($executed);
        $this->assertDatabaseMissing("foos", [ "name" => "Baz" ]);
        $this->assertDatabaseHas("foos", [ "name" => "Bar" ]);
    }
}
