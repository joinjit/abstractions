<?php

namespace Jit\AbstrActions\Tests\Unit;

use Jit\AbstrActions\Tests\Actions\Delete\CrashingTestDeleteAction;
use Jit\AbstrActions\Tests\Actions\Delete\TestDeleteAction;
use Jit\AbstrActions\Tests\CreatesFoo;
use Jit\AbstrActions\Tests\TestCase;

class DeleteActionTest extends TestCase
{
    use CreatesFoo;

    /** @test */
    public function it_deletes_a_model_successfully()
    {
        $foo = $this->createFoo();

        $executed = (new TestDeleteAction())->execute($foo);

        $this->assertTrue($executed);
        $this->assertDatabaseMissing("foos", [ "name" => "Bar" ]);
    }

    /** @test */
    public function it_does_not_execute_if_an_exception_is_thrown()
    {
        $foo = $this->createFoo();

        $executed = (new CrashingTestDeleteAction())->execute($foo);

        $this->assertFalse($executed);
        $this->assertDatabaseHas("foos", [ "name" => "Bar" ]);
    }
}
