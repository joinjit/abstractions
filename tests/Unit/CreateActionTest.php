<?php

namespace Jit\AbstrActions\Tests\Unit;

use Jit\AbstrActions\Tests\Actions\Create\CrashingTestCreateAction;
use Jit\AbstrActions\Tests\Actions\Create\TestCreateAction;
use Jit\AbstrActions\Tests\TestCase;

class CreateActionTest extends TestCase
{
    /** @test */
    public function it_executes_successfully()
    {
        $executed = (new TestCreateAction())->execute([ "name" => "Bar" ]);

        $this->assertTrue($executed);
        $this->assertDatabaseHas("foos", [ "name" => "Bar" ]);
    }

    /** @test */
    public function it_does_not_execute_if_no_data_is_provided()
    {
        $this->assertFalse((new TestCreateAction())->execute());
    }

    /** @test  */
    public function it_does_not_execute_if_data_is_empty()
    {
        $this->assertFalse((new TestCreateAction())->execute());
    }

    /** @test */
    public function it_does_not_execute_if_an_exception_is_thrown()
    {
        $this->assertFalse(
            (new CrashingTestCreateAction())->execute([ "name" => "Bar" ])
        );
    }
}
