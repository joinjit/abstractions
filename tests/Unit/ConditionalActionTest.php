<?php

namespace Jit\AbstrActions\Tests\Unit;

use Jit\AbstrActions\Tests\Actions\Conditional\FailingCreateAction;
use Jit\AbstrActions\Tests\Actions\Conditional\FailingDeleteAction;
use Jit\AbstrActions\Tests\Actions\Conditional\FailingUpdateAction;
use Jit\AbstrActions\Tests\Actions\Conditional\PassingCreateAction;
use Jit\AbstrActions\Tests\Actions\Conditional\PassingDeleteAction;
use Jit\AbstrActions\Tests\Actions\Conditional\PassingUpdateAction;
use Jit\AbstrActions\Tests\CreatesFoo;
use Jit\AbstrActions\Tests\TestCase;

class ConditionalActionTest extends TestCase
{
    use CreatesFoo;

    /** @test */
    public function conditional_create_action_passes_if_condition_is_true()
    {
        $executed = (new PassingCreateAction())->execute([ "name" => "Bar" ]);

        $this->assertTrue($executed);
        $this->assertDatabaseHas("foos", [ "name" => "Bar" ]);
    }

    /** @test */
    public function conditional_create_action_fails_if_condition_is_false()
    {
        $executed = (new FailingCreateAction())->execute([ "name" => "Bar" ]);

        $this->assertFalse($executed);
        $this->assertDatabaseMissing("foos", [ "name" => "Bar" ]);
    }

    /** @test */
    public function conditional_update_action_passes_if_condition_is_true()
    {
        $foo = $this->createFoo();

        $executed = (new PassingUpdateAction())
            ->execute($foo, [ "name" => "Baz" ]);

        $this->assertTrue($executed);
        $this->assertDatabaseMissing("foos", [ "name" => "Bar" ]);
        $this->assertDatabaseHas("foos", [ "name" => "Baz" ]);
    }

    /** @test */
    public function conditional_update_action_fails_if_condition_is_false()
    {
        $foo = $this->createFoo();

        $executed = (new FailingUpdateAction())
            ->execute($foo, [ "name" => "Baz" ]);

        $this->assertFalse($executed);
        $this->assertDatabaseMissing("foos", [ "name" => "Baz" ]);
        $this->assertDatabaseHas("foos", [ "name" => "Bar" ]);
    }

    /** @test */
    public function conditional_delete_action_passes_if_condition_is_true()
    {
        $foo = $this->createFoo();

        $executed = (new PassingDeleteAction())->execute($foo);

        $this->assertTrue($executed);
        $this->assertDatabaseMissing("foos", [ "name" => "Bar" ]);
    }

    /** @test */
    public function conditional_delete_action_fails_if_condition_is_false()
    {
        $foo = $this->createFoo();

        $executed = (new FailingDeleteAction())->execute($foo);

        $this->assertFalse($executed);
        $this->assertDatabaseHas("foos", [ "name" => "Bar" ]);
    }
}
