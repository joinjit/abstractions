<?php

namespace Jit\AbstrActions\Tests\Actions\Create;

use Illuminate\Database\Eloquent\Model;
use Jit\AbstrActions\Actions\AbstractCreateAction;
use Jit\AbstrActions\Tests\Models\Foo;

class TestCreateAction extends AbstractCreateAction
{
    protected function create(array $data): Model
    {
        return Foo::create($data);
    }
}
