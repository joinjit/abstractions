<?php

namespace Jit\AbstrActions\Tests\Actions\Create;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Jit\AbstrActions\Actions\AbstractCreateAction;

class CrashingTestCreateAction extends AbstractCreateAction
{
    protected function create(array $data): Model
    {
        throw new Exception("Dummy exception.");
    }
}
