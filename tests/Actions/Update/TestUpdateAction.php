<?php

namespace Jit\AbstrActions\Tests\Actions\Update;

use Illuminate\Database\Eloquent\Model;
use Jit\AbstrActions\Actions\AbstractUpdateAction;

class TestUpdateAction extends AbstractUpdateAction
{
    protected function update(Model $model, array $data = [])
    {
        $model->update($data);
    }
}
