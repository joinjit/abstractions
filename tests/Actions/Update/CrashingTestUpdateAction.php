<?php

namespace Jit\AbstrActions\Tests\Actions\Update;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Jit\AbstrActions\Actions\AbstractUpdateAction;

class CrashingTestUpdateAction extends AbstractUpdateAction
{
    protected function update(Model $model, array $data = [])
    {
        throw new Exception("Dummy exception.");
    }
}
