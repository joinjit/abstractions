<?php

namespace Jit\AbstrActions\Tests\Actions\Conditional;

use Illuminate\Database\Eloquent\Model;
use Jit\AbstrActions\Actions\AbstractDeleteAction;
use Jit\AbstrActions\Contracts\ConditionalAction;

class PassingDeleteAction extends AbstractDeleteAction implements ConditionalAction
{
    protected function delete(Model $model, array $data = [])
    {
        $model->delete();
    }

    public function passes(): bool
    {
        return true;
    }
}
