<?php

namespace Jit\AbstrActions\Tests\Actions\Conditional;

use Illuminate\Database\Eloquent\Model;
use Jit\AbstrActions\Actions\AbstractCreateAction;
use Jit\AbstrActions\Contracts\ConditionalAction;
use Jit\AbstrActions\Tests\Models\Foo;

class PassingCreateAction extends AbstractCreateAction implements ConditionalAction
{
    protected function create(array $data): Model
    {
        return Foo::create($data);
    }

    public function passes(): bool
    {
        return true;
    }
}
