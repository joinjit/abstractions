<?php

namespace Jit\AbstrActions\Tests\Actions\Conditional;

use Illuminate\Database\Eloquent\Model;
use Jit\AbstrActions\Actions\AbstractUpdateAction;
use Jit\AbstrActions\Contracts\ConditionalAction;

class PassingUpdateAction extends AbstractUpdateAction implements ConditionalAction
{
    protected function update(Model $model, array $data = [])
    {
        $model->update($data);
    }

    public function passes(): bool
    {
        return true;
    }
}
