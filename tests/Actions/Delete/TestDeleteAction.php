<?php

namespace Jit\AbstrActions\Tests\Actions\Delete;

use Illuminate\Database\Eloquent\Model;
use Jit\AbstrActions\Actions\AbstractDeleteAction;

class TestDeleteAction extends AbstractDeleteAction
{
    protected function delete(Model $model, array $data = [])
    {
        $model->delete();
    }
}
