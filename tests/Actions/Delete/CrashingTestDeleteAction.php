<?php

namespace Jit\AbstrActions\Tests\Actions\Delete;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Jit\AbstrActions\Actions\AbstractDeleteAction;

class CrashingTestDeleteAction extends AbstractDeleteAction
{
    protected function delete(Model $model, array $data = [])
    {
        throw new Exception("Dummy exception.");
    }
}
