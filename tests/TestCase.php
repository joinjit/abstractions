<?php

namespace Jit\AbstrActions\Tests;

use Illuminate\Database\Schema\Blueprint;
use Jit\AbstrActions\AbstrActionsServiceProvider;
use Orchestra\Testbench\TestCase as OrchestraTestCase;

class TestCase extends OrchestraTestCase
{
    protected function getPackageProviders($app)
    {
        return [ AbstrActionsServiceProvider::class ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', "testbench");
        $app['config']->set("database.connections.testbench", [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);
    }

    protected function setUpDatabase(): void
    {
        $this->app['db']->connection()
            ->getSchemaBuilder()
            ->create('foos', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->timestamps();
            });
    }

    public function setUp(): void
    {
        parent::setUp();

        $this->setUpDatabase();
    }
}
