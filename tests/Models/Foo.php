<?php

namespace Jit\AbstrActions\Tests\Models;

use Illuminate\Database\Eloquent\Model;

class Foo extends Model
{
    protected $table = "foos";

    protected $fillable = [
        "name"
    ];
}
