# AbstrActions

AbstrActions is a package that offers abstract actions with simplified features such as adding conditionals.

## Installation

You can install the package via composer:

```bash
composer require jit/abstractions
```

## Usage

Below is a quick guid on how to use AbstrActions.

### Abstract Create Actions

Abstract create actions are actions used to create and persist a model in the database. In order to use abstract create actions, you must:
1. Use the `AbstractCreateAction` from `Optidist\AbstrActions\Actions\AbstractCreateAction`;
2. Extend it.
3. Implement the `create(array $data = [])` method.
4. Execute the action by calling the `execute(array $data): bool` method which returns a boolean to state whether the action has executed or not.

``` php

// ConcreteCreateAction.php

[...]
use Jit\AbstrActions\Actions\AbstractCreateAction;

class ConcreteCreateAction extends AbstractCreateAction
{

    protected function create(array $data = []): Model
    {
        // Perform action here with the available data that is passed upon execution.
        // For example,
        return Foo::create($data);
    }
}

// FooController.php

[...]
use ConcreteCreateAction;

class FooController
{
    [...]
    public function store()
    {
        $executed = (new ConcreteCreateAction())->execute([ "name" => "Bar" ]);
    }
}
```

### Abstract Update Actions

Abstract update actions are actions used to update an existing model in the database. In order to use abstract update actions, you must:
1. Use the `AbstractUpdateAction` from `Optidist\AbstrActions\Actions\AbstractUpdateAction`;
2. Extend it.
3. Implement the `update(Model $model, array $data = [])` method.
4. Execute the action by calling the `execute(Model $model, array $data): bool` method which returns a boolean to state whether the action has executed or not.

``` php

// ConcreteUpdateAction.php

[...]
use Jit\AbstrActions\Actions\AbstractUpdateAction;

class ConcreteUpdateAction extends AbstractUpdateAction
{
    protected function update(Model $model, array $data = [])
    {
        // Perform action here with the available model and data that is passed upon execution.
        // For example,
        $model->update($data);
    }
}

// FooController.php

[...]
use ConcreteUpdateAction;

class FooController
{
    [...]
    public function update(Foo $foo)
    {
        $executed = (new ConcreteUpdateAction())->execute($foo, [ "name" => "Bar" ]);
    }
}
```

### Abstract Delete Actions

Abstract delete actions are actions used to delete an existing model in the database. In order to use abstract delete actions, you must:
1. Use the `AbstractDeleteAction` from `Optidist\AbstrActions\Actions\AbstractDeleteAction`;
2. Extend it.
3. Implement the `delete(Model $model, array $data = [])` method.
4. Execute the action by calling the `execute(Model $model, array $data = []): bool` method which returns a boolean to state whether the action has executed or not.

``` php

// ConcreteDeleteAction.php

[...]
use Jit\AbstrActions\Actions\AbstractDeleteAction;

class ConcreteDeleteAction extends AbstractDeleteAction
{
    protected function delete(Model $model, array $data = [])
    {
        // Perform action here with the available model and optional data that is passed upon execution.
        // For example,
        $model->delete();
    }
}

// FooController.php

[...]
use ConcreteDeleteAction;

class FooController
{
    [...]
    public function delete(Foo $foo)
    {
        $executed = (new ConcreteDeleteAction())->execute($foo);
    }
}
```

### Conditional Actions

Conditional actions are executed only when a specified condition passes. You can use make any abstract action conditional by:
1. Using the `ConditionalAction` interface from `Optidist\AbstrActions\Contracts\ConditionalAction;
2. Implementing the interface.
3. Implementing the `passes(): bool` method.

```php
// TestAction.php

[...]

use Jit\AbstrActions\Actions\AbstractCreateAction;
use Jit\AbstrActions\Contracts\ConditionalAction;

class TestAction extends AbstractCreateAction implements ConditionalAction
{
    protected function create(array $data = []): Model
    {
        [...]
    }

    public function passes(): bool
    {
        // Action will execute only if $condition is true.
        return $condition;
    }
}
```

### Auditing Abstract Actions
You can audit abstract actions after executing them by doing the following:
1. Use the `AuditAfter` interface from `Optidist\AbstrActions\Contracts\AuditAfter`;
2. Implement the `audit(Model $model, array $data = [])` method.

#### Example
```php
// ConcreteCreateAction.php

[...]
use Jit\AbstrActions\Contracts\AuditAfter;

class ConcreteCreateAction extends AbstractCreateAction implements AuditAfter
{
    protected function create(array $data = []): Model
    {
        // Create and return model instance.
    }

    public function audit(Model $model, array $data = [])
    {
        // Do something here...
    }
}
``` 

### Testing

``` bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

### Security

If you discover any security related issues, please email j.karam@joinjit.com instead of using the issue tracker.

## Credits

- [Joe Karam](https://github.com/joekaram)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
