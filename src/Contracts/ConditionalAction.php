<?php

namespace Jit\AbstrActions\Contracts;

interface ConditionalAction
{
    public function passes(): bool;
}
