<?php

namespace Jit\AbstrActions\Contracts;

use Illuminate\Database\Eloquent\Model;

interface AuditAfter
{
    public function audit(Model $model, array $data = []);
}
