<?php

namespace Jit\AbstrActions\Actions;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Jit\AbstrActions\Contracts\ConditionalAction;
use Jit\AbstrActions\Traits\ActionAudits;

abstract class AbstractDeleteAction
{
    use ActionAudits;

    private function applyChecks()
    {
        if ($this instanceof ConditionalAction && !$this->passes()) {
            throw new Exception("Passing condition was not satisfied.");
        }
    }

    public function execute(Model $model, array $data = [])
    {
        try {
            $this->applyChecks();
            $this->delete($model, $data);
            $this->auditAfter($model, $data);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    abstract protected function delete(Model $model, array $data = []);
}
