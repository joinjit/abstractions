<?php

namespace Jit\AbstrActions\Actions;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Jit\AbstrActions\Contracts\ConditionalAction;
use Jit\AbstrActions\Traits\ActionAudits;

abstract class AbstractCreateAction
{
    use ActionAudits;

    private function applyChecks(array $data)
    {
        if (!count($data)) {
            throw new Exception("Data passed to execute action is invalid.");
        }

        if ($this instanceof ConditionalAction && !$this->passes()) {
            throw new Exception("Passing condition was not satisfied.");
        }
    }

    public function execute(array $data = []): bool
    {
        try {
            $this->applyChecks($data);
            $model = $this->create($data);
            $this->auditAfter($model, $data);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    abstract protected function create(array $data): Model;
}
