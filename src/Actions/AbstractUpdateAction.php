<?php

namespace Jit\AbstrActions\Actions;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Jit\AbstrActions\Contracts\ConditionalAction;
use Jit\AbstrActions\Traits\ActionAudits;

abstract class AbstractUpdateAction
{
    use ActionAudits;

    private function applyChecks(array $data)
    {
        if (!count($data)) {
            throw new Exception("Data passed to execute action is invalid.");
        }

        if ($this instanceof ConditionalAction && !$this->passes()) {
            throw new Exception("Passing condition was not satisfied.");
        }
    }

    public function execute(Model $model, array $data = [])
    {
        try {
            $this->applyChecks($data);
            $this->update($model, $data);
            $this->auditAfter($model, $data);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    abstract protected function update(Model $model, array $data = []);
}
