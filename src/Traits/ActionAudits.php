<?php

namespace Jit\AbstrActions\Traits;

use Illuminate\Database\Eloquent\Model;
use Jit\AbstrActions\Contracts\AuditAfter;

trait ActionAudits
{
    protected function auditAfter(Model $model, array $data = [])
    {
        if ($this instanceof AuditAfter) {
            $this->audit($model, $data);
        }
    }
}
