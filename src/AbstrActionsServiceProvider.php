<?php

namespace Jit\AbstrActions;

use Illuminate\Support\ServiceProvider;

class AbstrActionsServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . "/../config/config.php" => config_path("abstractions.php"),
            ], 'config');
        }
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . "/../config/config.php",
            "abstractions"
        );
    }
}
